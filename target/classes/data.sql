insert into app_user(id,email,username,password,role) values (2,'admin@admin.com','admin','admin','ADMIN');
insert into job(id,title,city,company,technology,level,salary_from,salary_to,active) values (1,'Junior Java Developer','Warsaw','Devis','Java','Junior',4500,5000, false);
insert into job(id,title,city,company,technology,level,salary_from,salary_to,active) values (3,'Java Developer','Cracov','RandomCompany', 'Python','Senior',7000,9500, true);
insert into job(id,title,city,company,technology,level,salary_from,salary_to,active) values (4,'Junior Python Developer','Cracov','Oasis System', 'Python','Junior',2000,3500, true);
insert into job(id,title,city,company,technology,level,salary_from,salary_to,active) values (5,'Java Developer','Katowice','Luwist Inc', 'Java','Mid',5000,7500, true);
insert into job(id,title,city,company,technology,level,salary_from,salary_to,active) values (6,'Regular Python Developer','Cracov','Orket Works', 'Python','Mid',2000,3500, true);
insert into job(id,title,city,company,technology,level,salary_from,salary_to,active) values (7,'Senior PHP Developer','Cracov','Orket Works', 'Python','Senior',8000,15000, true);
insert into job(id,title,city,company,technology,level,salary_from,salary_to,active) values (8,'Senior PHP Developer','Warsaw','Italumisis Systems', 'Python','Senior',17000,20000, true);
insert into job(id,title,city,company,technology,level,salary_from,salary_to,active) values (9,'Regular PHP Developer','Cracov','Luwist Inc', 'PHP','Mid',5500,6500, true);
insert into job(id,title,city,company,technology,level,salary_from,salary_to,active) values (10,'Junior PHP Developer','Katowice','Kordic Systems', 'PHP','Junior',2500,4000, false);
insert into job(id,title,city,company,technology,level,salary_from,salary_to,active) values (11,'Senior Python Developer','Wroclaw','Dager Works', 'Python','Senior',12000,15000, true);
insert into job(id,title,city,company,technology,level,salary_from,salary_to,active) values (12,'Regular Java Developer','Warsaw','Fallist Inc', 'Java','Mid',7000,9000, true);
insert into job(id,title,city,company,technology,level,salary_from,salary_to,active) values (13,'Junior C++ Developer','Cracov','Halley Systems', 'C++','Junior',4000,5000, true);
insert into job(id,title,city,company,technology,level,salary_from,salary_to,active) values (14,'PHP Developer','Sopot','Forcet Works', 'PHP','Mid',8000,9000, true);
insert into job(id,title,city,company,technology,level,salary_from,salary_to,active) values (15,'Senior Java Developer','Cracov','Serrist', 'Java','Senior',13000,1400, true);
insert into job(id,title,city,company,technology,level,salary_from,salary_to,active) values (16,'Junior PHP Developer','Sopot','Groven', 'PHP','Junior',2000,2500, true);




