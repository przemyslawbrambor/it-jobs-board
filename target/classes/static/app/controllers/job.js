angular.module('ITJobsBoard')
.controller('JobController', function($http, $scope, AuthService, filterFilter, $window) {
        var edit = false;

    $scope.buttonText = 'Create';
        var init = function() {
            $http.get('api/job').success(function(res) {
                $scope.jobList = res;
            }).error(function(error) {
                $scope.message = error.message;
            });
        };





        $scope.initEdit = function(job) {
            edit = true;
            $scope.job = job;
            $scope.message='';
            $scope.deleteMessage='';
            $scope.buttonText = 'Update';
        };
        $scope.initAddJob = function() {
            edit = false;
            $scope.job = null;
            $scope.message='';
            $scope.deleteMessage='';
            $scope.buttonText = 'Create';
        };
        $scope.deleteJob = function(job) {
            $http.delete('api/job/'+job.id).success(function(res) {
                $scope.deleteMessage ="Success!";
                init();
            }).error(function(error) {
                $scope.deleteMessage = error.message;
            });
        };
        $scope.searchJob = function(job) {
            $http.get('api/search/'+job.searchCity).success(function(res) {
                $scope.deleteMessage ="Success!";
                init();
            }).error(function(error) {
                $scope.deleteMessage = error.message;
            });
        };
        var editJob = function(){
            $http.put('api/job', $scope.job).success(function(res) {
                $scope.job = null;
                $scope.confirmPassword = null;
                $scope.jobForm.$setPristine();
                $scope.message = "Editting Success";
                init();
            }).error(function(error) {
                $scope.message =error.message;
            });
        };
        var addJob = function(){
            $http.post('api/job', $scope.job).success(function(res) {
                $scope.job =  null;
                $scope.confirmPassword = null;
                $scope.jobForm.$setPristine();
                $scope.message = "Job Created";
                init();
            }).error(function(error) {
                $scope.message = error.message;
            });
        };
        $scope.submit = function() {
            if(edit){
                editJob();
            }else{
                addJob();
            }
        };

        function Ctrl($scope) {
            $scope.job = {
                isActive: true,
                course: 'chemistry'
            };
        };
        init();

    $scope.cities = ['Wroclaw', 'Cracov', 'Warsaw', 'Sopot', 'Katowice'];
    $scope.levels = ['Junior', 'Mid', 'Senior'];
    $scope.technologies = ['PHP', 'Java', 'Python', 'C++'];

    $scope.citySelection = [];
    $scope.levelSelection = [];
    $scope.technologySelection = [];
    $scope.selection = [];

    


    $scope.toggleCitySelection = function toggleCitySelection(item) {
        var idx = $scope.citySelection.indexOf(item);

        if (idx > -1) {
            $scope.citySelection.splice(idx, 1);
        }

        else {
            $scope.citySelection.push(item);
        }
    }


    $scope.toggleTechnologySelection = function toggleTechnologySelection(item) {
        var idx = $scope.technologySelection.indexOf(item);

        // Is currently selected
        if (idx > -1) {
            $scope.technologySelection.splice(idx, 1);
        }

        // Is newly selected
        else {
            $scope.technologySelection.push(item);
        }
    }

    $scope.toggleLevelSelection = function toggleLevelSelection(item) {
        var idx = $scope.levelSelection.indexOf(item);

        if (idx > -1) {
            $scope.levelSelection.splice(idx, 1);
        }

        else {
            $scope.levelSelection.push(item);
        }
    }

})
    .filter('customArray', function($filter){
        return function(list, arrayFilter, element){
            if(arrayFilter){
                return $filter("filter")(list, function(listItem){
                    return arrayFilter.indexOf(listItem[element]) != -1;
                });
            }
        };
    })
    .filter('unique', function() {

    return function(collection, keyname) {
        var output = [],
            keys = [];
        angular.forEach(collection, function(item) {
            var key = item[keyname];
            if(keys.indexOf(key) === -1) {
                keys.push(key);
                output.push(item);
            }
        });

        return output;
    };
});

