package pl.przemyslawbrambor.ITJobsBoard.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import pl.przemyslawbrambor.ITJobsBoard.domain.Job;

public interface JobRepository extends JpaRepository<Job, Long> {
    public Job findOneByTitle(String title);

}