package pl.przemyslawbrambor.ITJobsBoard.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import pl.przemyslawbrambor.ITJobsBoard.domain.AppUser;

public interface AppUserRepository extends JpaRepository<AppUser, Long> {
	public AppUser findOneByUsername(String username);
}
