package pl.przemyslawbrambor.ITJobsBoard.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Job {


    @Id
    @GeneratedValue
    private Long id;

    private String title;
    private String city;
    private String company;
    private String technology;
    private String level;
    private double salaryFrom;
    private double salaryTo;
    private boolean active;

    public Job() { }

    public Job(String title, String city, String company, String technology, String level, double salaryFrom, double salaryTo, boolean active) {
        this.title = title;
        this.city = city;
        this.company = company;
        this.technology = technology;
        this.level = level;
        this.salaryFrom = salaryFrom;
        this.salaryTo = salaryTo;
        this.active = active;
    }

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public String getTitle() { return title; }

    public void setTitle(String title) { this.title = title; }

    public String getCity() { return city; }

    public void setCity(String city) { this.city = city; }

    public String getCompany() { return company; }

    public void setCompany(String company) { this.company = company; }

    public String getTechnology() { return technology; }

    public void setTechnology(String technology) { this.technology = technology; }

    public String getLevel() { return level; }

    public void setLevel(String level) { this.level = level; }

    public double getSalaryFrom() { return salaryFrom; }

    public void setSalaryFrom(double salaryFrom) { this.salaryFrom = salaryFrom; }

    public double getSalaryTo() { return salaryTo; }

    public void setSalaryTo(double salaryTo) { this.salaryTo = salaryTo; }

    public boolean isActive() { return active; }

    public void setActive(boolean active) { this.active = active; }

}
