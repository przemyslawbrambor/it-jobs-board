package pl.przemyslawbrambor.ITJobsBoard.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.przemyslawbrambor.ITJobsBoard.repository.JobRepository;
import pl.przemyslawbrambor.ITJobsBoard.domain.Job;

import java.util.List;


@RestController
@RequestMapping(value = "/api")
public class JobController {

    @Autowired
    JobRepository jobRepository;

    @RequestMapping(value = "/job", method = RequestMethod.GET)
    public List<Job> job() { return jobRepository.findAll(); }

    @RequestMapping(value = "/job/{id}", method = RequestMethod.GET)
    public ResponseEntity<Job> jobById(@PathVariable Long id) {
        Job job = jobRepository.findOne(id);
        if (job == null) {
            return new ResponseEntity<Job>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<Job>(job, HttpStatus.OK);
        }

    }

    @RequestMapping(value = "/job/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Job> deleteJob(@PathVariable Long id) {
        Job job = jobRepository.findOne(id);
        if (job == null) {
            return new ResponseEntity<Job>(HttpStatus.NO_CONTENT);
        } else {
            jobRepository.delete(job);
                return new ResponseEntity<Job>(job, HttpStatus.OK);
        }

    }

    @RequestMapping(value = "/job", method = RequestMethod.POST)
    public ResponseEntity<Job> createJob(@RequestBody Job job) {
        return new ResponseEntity<Job>(jobRepository.save(job), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/job", method = RequestMethod.PUT)
    public Job updateJob(@RequestBody Job job) {
        System.out.println(job.getCity());
        System.out.println(job.getCompany());
        System.out.println(job.getTitle());
        return jobRepository.save(job);
    }}
