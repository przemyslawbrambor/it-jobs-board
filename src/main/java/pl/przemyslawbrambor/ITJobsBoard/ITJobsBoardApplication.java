package pl.przemyslawbrambor.ITJobsBoard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class ITJobsBoardApplication {


	public static void main(String[] args) {
		SpringApplication.run(ITJobsBoardApplication.class, args); }
}
